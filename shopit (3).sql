-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2018 at 12:56 PM
-- Server version: 5.6.25
-- PHP Version: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopit`
--

-- --------------------------------------------------------

--
-- Table structure for table `shopitadmins`
--

CREATE TABLE IF NOT EXISTS `shopitadmins` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` text,
  `hash` text,
  `activated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitadmins`
--

INSERT INTO `shopitadmins` (`id`, `name`, `email`, `password`, `hash`, `activated`) VALUES
(3, 'steve', 'almond@gmail.com', '$2y$10$3yxPSdMou18cuPTEY85c7Or.k2INs7dYs45GVCHCtXfzG7JllrvFO', '$2y$10$3yxPSdMou18cuPTEY85c7Or.k2INs7dYs45GVCHCtXfzG7JllrvFO', 1),
(4, 'michelle', 'mwendemich@gmail.com', '$2y$10$ikBaRhrIqTViArtYAFytJOCG9acRLBkgrfNTAqUmwqbhiIQ6cWzym', '$2y$10$ikBaRhrIqTViArtYAFytJOCG9acRLBkgrfNTAqUmwqbhiIQ6cWzym', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shopitagents`
--

CREATE TABLE IF NOT EXISTS `shopitagents` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone_number` text NOT NULL,
  `idnumber` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `hash` text NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `dateemployed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitagents`
--

INSERT INTO `shopitagents` (`id`, `name`, `phone_number`, `idnumber`, `email`, `password`, `hash`, `activated`, `dateemployed`) VALUES
(1, 'steve', '0702653268', '34556470', 'almond@gmail.com', '$2y$10$zb.jIo.ktG8Mp73MhdknCeJtq.8UB4KQ1nQcMToLo05uObO4VxRLK', '$2y$10$zb.jIo.ktG8Mp73MhdknCeJtq.8UB4KQ1nQcMToLo05uObO4VxRLK', 1, '2018-09-25 22:21:51');

-- --------------------------------------------------------

--
-- Table structure for table `shopitcategories`
--

CREATE TABLE IF NOT EXISTS `shopitcategories` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `pic` blob NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitcategories`
--

INSERT INTO `shopitcategories` (`id`, `name`, `pic`) VALUES
(4, 'fruits,veges&ingredients', ''),
(5, 'drinks', ''),
(6, 'snacks', '');

-- --------------------------------------------------------

--
-- Table structure for table `shopitcustomers`
--

CREATE TABLE IF NOT EXISTS `shopitcustomers` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` text,
  `hash` text,
  `activated` tinyint(1) DEFAULT '0',
  `phone` text,
  `address` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitcustomers`
--

INSERT INTO `shopitcustomers` (`id`, `name`, `email`, `password`, `hash`, `activated`, `phone`, `address`) VALUES
(1, 'steve', 'almond@gmail.com', '$2y$10$RYftJ5KHhfnv9uP6gF9jyuyuyEZ91xiL.b116ApIB2IL66R2AkM7W', '$2y$10$RYftJ5KHhfnv9uP6gF9jyuyuyEZ91xiL.b116ApIB2IL66R2AkM7W', 1, NULL, 'juja,taraja heights');

-- --------------------------------------------------------

--
-- Table structure for table `shopititems`
--

CREATE TABLE IF NOT EXISTS `shopititems` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `pic` blob NOT NULL,
  `discount` int(11) NOT NULL,
  `Uploader` int(11) NOT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` text,
  `identifier` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopititems`
--

INSERT INTO `shopititems` (`id`, `name`, `pic`, `discount`, `Uploader`, `categoryid`, `price`, `description`, `identifier`) VALUES
(16, 'LEGEND', 0x75706c6f61646564696e666f2f313533373734323332316c6567656e64732e6a706567, 3, 3, 5, 150, 'LIQUOR', '53453456456trdfbdf'),
(17, 'WINE', 0x75706c6f61646564696e666f2f3135333737343236323077696e652e6a7067, 0, 3, 5, 456, 'RED WINE', 'wqrfqwejhru45278r7gdf'),
(18, 'HOHO', 0x75706c6f61646564696e666f2f31353337373432363632686f686f2e6a7067, 5, 3, 4, 100, 'VEGETABLE', 'dfjkgeruityeruiyt89ert78rhfgieh'),
(19, 'COOKIES', 0x75706c6f61646564696e666f2f31353337373432363931636f6f6b6965732e6a7067, 10, 3, 6, 4000, 'SWEET COOKIES', 'sdfgmfgfgfgfgrt6e78tefgkghdfiy'),
(20, 'YOGHURT', 0x75706c6f61646564696e666f2f31353337373432373237796f67687572742e6a7067, 0, 3, 5, 278, 'MILK PRODUCT', 'sdfgsduyfrtgeut78ertfg7tfg7tfg7tfg7tfg7iueri'),
(21, 'GRAPES', 0x75706c6f61646564696e666f2f313533373831303231306772617065732e6a7067, 2, 3, 4, 20, 'GRAPES', '$2y$10$skkzgJw7ZFg0pwm.S2tZ..3oa18DZdfHy7Bvq4Qp5cr.uB49cdd3O'),
(22, 'UNDIES', 0x75706c6f61646564696e666f2f3135333739393239393170726f647563742d372e6a7067, 0, 3, 6, 23432, 'UNDERPANTS', '$2y$10$ver8WwEhl8Hn4sBc1yYEl.KNaYLhLUJw19XUelbL1XSl0x7l3z216');

-- --------------------------------------------------------

--
-- Table structure for table `shopitorders`
--

CREATE TABLE IF NOT EXISTS `shopitorders` (
  `id` int(11) NOT NULL,
  `agent` text NOT NULL,
  `dateordered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `datecompleted` datetime NOT NULL,
  `items` text NOT NULL,
  `customer` int(11) NOT NULL,
  `customerpaymentid` text NOT NULL,
  `shopittransactionid` text NOT NULL,
  `transactioncomplete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitorders`
--

INSERT INTO `shopitorders` (`id`, `agent`, `dateordered`, `datecompleted`, `items`, `customer`, `customerpaymentid`, `shopittransactionid`, `transactioncomplete`) VALUES
(1, '', '2018-10-22 10:56:31', '0000-00-00 00:00:00', '{"$2y$10$skkzgJw7ZFg0pwm.S2tZ..3oa18DZdfHy7Bvq4Qp5cr.uB49cdd3O":{"name":"GRAPES","discount":"2","identifier":"$2y$10$skkzgJw7ZFg0pwm.S2tZ..3oa18DZdfHy7Bvq4Qp5cr.uB49cdd3O","description":"GRAPES","quantity":"1","price":"20","pic":"uploadedinfo\\/1537810210grapes.jpg"}}', 0, '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shopitadmins`
--
ALTER TABLE `shopitadmins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopitagents`
--
ALTER TABLE `shopitagents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idnumber` (`idnumber`(9)),
  ADD UNIQUE KEY `companyid` (`email`(10));

--
-- Indexes for table `shopitcategories`
--
ALTER TABLE `shopitcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopitcustomers`
--
ALTER TABLE `shopitcustomers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopititems`
--
ALTER TABLE `shopititems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Uploader` (`Uploader`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `shopitorders`
--
ALTER TABLE `shopitorders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `custpaymentid` (`customerpaymentid`(40));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shopitadmins`
--
ALTER TABLE `shopitadmins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shopitagents`
--
ALTER TABLE `shopitagents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shopitcategories`
--
ALTER TABLE `shopitcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `shopitcustomers`
--
ALTER TABLE `shopitcustomers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shopititems`
--
ALTER TABLE `shopititems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `shopitorders`
--
ALTER TABLE `shopitorders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `shopititems`
--
ALTER TABLE `shopititems`
  ADD CONSTRAINT `shopititems_ibfk_1` FOREIGN KEY (`Uploader`) REFERENCES `shopitadmins` (`id`),
  ADD CONSTRAINT `shopititems_ibfk_2` FOREIGN KEY (`categoryid`) REFERENCES `shopitcategories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
