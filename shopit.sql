-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2019 at 11:03 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopit`
--

-- --------------------------------------------------------

--
-- Table structure for table `shopitadmins`
--

CREATE TABLE `shopitadmins` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` text,
  `hash` text,
  `activated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitadmins`
--

INSERT INTO `shopitadmins` (`id`, `name`, `email`, `password`, `hash`, `activated`) VALUES
(1, 'admin', 'almond@gmail.com', '$2y$10$GGg6D1atzLRNkQ.la5RJLOazZALlCdgGjl7ntFPmrYqDAu6.mTh1K', '$2y$10$GGg6D1atzLRNkQ.la5RJLOazZALlCdgGjl7ntFPmrYqDAu6.mTh1K', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shopitagents`
--

CREATE TABLE `shopitagents` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone_number` text NOT NULL,
  `idnumber` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `hash` text NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `dateemployed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shopitcategories`
--

CREATE TABLE `shopitcategories` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `pic` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitcategories`
--

INSERT INTO `shopitcategories` (`id`, `name`, `pic`) VALUES
(1, 'SNACKS', ''),
(2, 'FRUITS & GROCERIES ', ''),
(3, 'DRINKS', '');

-- --------------------------------------------------------

--
-- Table structure for table `shopitcustomers`
--

CREATE TABLE `shopitcustomers` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` text,
  `hash` text,
  `activated` tinyint(1) DEFAULT '0',
  `phone` text,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitcustomers`
--

INSERT INTO `shopitcustomers` (`id`, `name`, `email`, `password`, `hash`, `activated`, `phone`, `address`) VALUES
(1, 'steve', 'almond@gmail.com', '$2y$10$b4ZmU.8HO4r1fpZ726t/oO4wponHtWIIy6DhgNCrQTHH7zw0cwOhm', '$2y$10$b4ZmU.8HO4r1fpZ726t/oO4wponHtWIIy6DhgNCrQTHH7zw0cwOhm', 1, NULL, '1234');

-- --------------------------------------------------------

--
-- Table structure for table `shopititems`
--

CREATE TABLE `shopititems` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `pic` blob NOT NULL,
  `discount` int(11) NOT NULL,
  `Uploader` int(11) NOT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` text,
  `identifier` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopititems`
--

INSERT INTO `shopititems` (`id`, `name`, `pic`, `discount`, `Uploader`, `categoryid`, `price`, `description`, `identifier`) VALUES
(3, 'coke', 0x75706c6f61646564696e666f2f31353532313335333730696e6465782e6a7067, 5, 1, 3, 150, 'a drink to remember', '$2y$10$6vSCK1ylH3KgoPw.JIYMsO907C.NlaBDZxltLd0dGCLFqK4WYBDA6');

-- --------------------------------------------------------

--
-- Table structure for table `shopitorders`
--

CREATE TABLE `shopitorders` (
  `id` int(11) NOT NULL,
  `agent` text NOT NULL,
  `dateordered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `datecompleted` datetime NOT NULL,
  `items` text NOT NULL,
  `customer` int(11) NOT NULL,
  `customerpaymentid` text NOT NULL,
  `shopittransactionid` text NOT NULL,
  `transactioncomplete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopitorders`
--

INSERT INTO `shopitorders` (`id`, `agent`, `dateordered`, `datecompleted`, `items`, `customer`, `customerpaymentid`, `shopittransactionid`, `transactioncomplete`) VALUES
(1, '', '2018-11-15 12:19:57', '0000-00-00 00:00:00', '{\"$2y$10$SCTpqt1n2rZictJjXhsz3OemsFmhumwq4vdPOGcI4JfYxrVxTxr96\":{\"name\":\"beer\",\"discount\":\"0\",\"identifier\":\"$2y$10$SCTpqt1n2rZictJjXhsz3OemsFmhumwq4vdPOGcI4JfYxrVxTxr96\",\"description\":\"a cold beer\",\"quantity\":\"100\",\"price\":\"345\",\"pic\":\"uploadedinfo\\/1542283305153695675259666836-under-construction-red-vector-under-construction-flat-vector-under-construction-background-under-con.jpg\"}}', 0, '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shopitadmins`
--
ALTER TABLE `shopitadmins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopitagents`
--
ALTER TABLE `shopitagents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idnumber` (`idnumber`(9)),
  ADD UNIQUE KEY `companyid` (`email`(10));

--
-- Indexes for table `shopitcategories`
--
ALTER TABLE `shopitcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopitcustomers`
--
ALTER TABLE `shopitcustomers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopititems`
--
ALTER TABLE `shopititems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Uploader` (`Uploader`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `shopitorders`
--
ALTER TABLE `shopitorders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `custpaymentid` (`customerpaymentid`(40));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shopitadmins`
--
ALTER TABLE `shopitadmins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shopitagents`
--
ALTER TABLE `shopitagents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shopitcategories`
--
ALTER TABLE `shopitcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shopitcustomers`
--
ALTER TABLE `shopitcustomers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shopititems`
--
ALTER TABLE `shopititems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shopitorders`
--
ALTER TABLE `shopitorders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
